var restify = require('restify');
var mongojs = require("mongojs");
var ObjectId = mongojs.ObjectId;

var ip_addr = '127.0.0.1';
var port    = '8080';

var server = restify.createServer({
	name : "Agenda-Server"
});

//server config
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());

//DB connection
var connection_string = 'mongodb://test:test@ds027789.mongolab.com:27789/contactsdb';
var db = mongojs(connection_string, ['contactsdb']);
var contacts = db.collection("contactscollection");

//API
var PATH = '/contacts'
server.get({path : PATH , version : '0.0.1'} , findAllContacts);
server.get({path : PATH +'/:contactId' , version : '0.0.1'} , findContact);
server.post({path : PATH , version: '0.0.1'} ,postNewContact);
server.del({path : PATH +'/:contactId' , version: '0.0.1'} ,deleteContact);
server.put({path : PATH +'/:contactId' , version: '0.0.1'} ,updateContact);

//Callbacks
function findAllContacts(req, res , next){
	console.log('Find All Contacts');
	res.setHeader('Access-Control-Allow-Origin','*');
	contacts.find({},function(err , success){		
		if(success){
			console.log('Response success '+success);
			res.send(200 , success);
			return next();
		}else{
			console.log('Response error '+err);
			return next(err);
		} 
	}); 
}

function findContact(req, res , next){
	console.log('Get Contact ID: ' + req.params.contactId);
	res.setHeader('Access-Control-Allow-Origin','*');	
	contacts.findOne({_id:ObjectId(req.params.contactId)} , function(err , success){				
		if(success){
			console.log('Response success '+success);					
			res.send(200 , success);
			return next();
		}
		return next(err);
	})
}

function postNewContact(req , res , next){
	console.log('Create Contact');
	var contact = req.body;	
	res.setHeader('Access-Control-Allow-Origin','*');
	contacts.save(contact , function(err , success){
		console.log('Response success '+success);
		console.log('Response error '+err);
		if(success){
			res.send(201 , contact);
			return next();
		}else{
			return next(err);
		}
	});
}

function updateContact(req , res , next){
	console.log('Update Contact ID: ' + req.params.contactId);	
	var contact = req.body;
	contact._id = new ObjectId(req.params.contactId);
	res.setHeader('Access-Control-Allow-Origin','*');
	contacts.save(contact, function(err , success){
		console.log('Response success '+success);
		console.log('Response error '+err);
		if(success){
			res.send(201 , contact);
			return next();
		}else{
			return next(err);
		}
	});
}

function deleteContact(req , res , next){
	console.log('Delete Contact ID: ' + req.params.contactId);
	res.setHeader('Access-Control-Allow-Origin','*');
	contacts.remove({_id:ObjectId(req.params.contactId)} , function(err , success){	
		console.log('Response success '+success);
		console.log('Response error '+err);
		if(success){
			res.send(204);
			return next();      
		} else{
			return next(err);
		}
	})
}

//Initialization
server.listen(port ,ip_addr, function(){
	console.log('%s listening at %s ', server.name , server.url);
});