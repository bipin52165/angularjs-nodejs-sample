'use strict';

// Declare app level module which depends on filters, and services
angular.module('contacts', [
	'ngRoute',		
	'contacts.services',	
	'contacts.controllers'
	]).
config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/listContacts', {templateUrl: 'partials/list.html', controller: 'ContactsCtrl'});	
	$routeProvider.when('/addContact', {templateUrl: 'partials/add.html', controller: 'ContactsCtrl'});
	$routeProvider.when('/editContact/:id', {templateUrl: 'partials/edit.html', controller: 'ContactsCtrl'});
	$routeProvider.when('/deleteContact/:id', {templateUrl: 'partials/delete.html', controller: 'ContactsCtrl'});
	$routeProvider.otherwise({redirectTo: '/listContacts'});
}]);
