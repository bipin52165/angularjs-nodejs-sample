'use strict';

/* Controllers */

angular.module('contacts.controllers', [])
.controller('ContactsCtrl', ['$scope','$location','$routeParams','Contact',
	function($scope, $location, $routeParams, Contact) {
		loadContacts();

		if($routeParams.id){
			$scope.contactModel = Contact.get({contactId:$routeParams.id});				
		}

		$scope.addContact = function() {			
			Contact.save($scope.newContactModel, backToList);
		}

		$scope.updateContact = function(contact) {
			contact.$update(backToList);
		}

		$scope.deleteContact = function(contact) {
			contact.$delete(function() {
				loadContacts();
				backToList();
			});
		}

		$scope.backToList = backToList;

		function backToList() {
			$location.path("listContacts");
		}

		function loadContacts() {
			$scope.contacts = Contact.query() 
		}
	}]);
