'use strict';

/* Services */

angular.module('contacts.services', ['ngResource']).
factory('Contact', ['$resource',
  function($resource){
    return $resource('http://localhost:8080/contacts/:contactId', {contactId:'@_id'}, {
      get:    {method:'GET'                          }, 	
      query:  {method:'GET', params:{}, isArray:true },
      save:   {method:'POST'                         },
      update: {method:'PUT'                          },
      delete: {method:'DELETE'                       }
    });
  }]);